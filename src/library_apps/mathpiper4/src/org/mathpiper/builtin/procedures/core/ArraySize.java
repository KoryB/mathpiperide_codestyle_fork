/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.Array;
import org.mathpiper.builtin.BuiltinContainer;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.cons.Cons;

/**
 *
 *  
 */
public class ArraySize extends BuiltinProcedure
{
    
    private ArraySize()
    {
    }

    public ArraySize(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Cons evaluated = getArgument(aEnvironment, aStackTop, 1);

        BuiltinContainer gen = (BuiltinContainer) evaluated.car();
        if(gen == null) LispError.checkArgument(aEnvironment, aStackTop, 1);
        if(! gen.typeName().equals("\"Array\"")) LispError.checkArgument(aEnvironment, aStackTop, 1);
        int size = ((Array) gen).size();
        setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aEnvironment.getPrecision(), "" + size));
    }
}//end class.



/*
%mathpiper_docs,name="ArraySize",categories="Programming Procedures,Native Objects,Built In"
*CMD ArraySize --- get array size
*CORE
*CALL
	ArraySize(array)

*DESC
Returns the size of an array (number of elements in the array).

PKHG TODO 18-11-2013
All 4 array alements ar[1] .. ar[4]

In> ar := ArrayCreate(4,[1 .. 4 ])
Result: class org.mathpiper.builtin.Array

In> ArraySize(ar)
Result: 4

In> ar[2]
Result: [[1,2,3,4]]

In> Type(ar[2])
Result: "List"

In> First(ar[2])
Result: [1,2,3,4]

In> First(ar[2])[4]
Result: 4


%/mathpiper_docs
*/
