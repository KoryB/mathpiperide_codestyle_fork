/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.io.InputStatus;
import org.mathpiper.io.StringOutputStream;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;

/**
 *
 *  
 */
public class PatchString extends BuiltinProcedure
{

    private PatchString()
    {
    }

    public PatchString(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
        String unpatchedString = 
        (String) getArgument(aEnvironment, aStackTop, 1).car();
        
        if(unpatchedString == null) LispError.checkArgument(aEnvironment, aStackTop, 2);
        
        InputStatus oldStatus = new InputStatus(aEnvironment.getCurrentInput().iStatus);
        aEnvironment.getCurrentInput().iStatus.setTo("STRING:USER");
        
        StringBuffer resultBuffer = new StringBuffer();
        StringOutputStream resultStream = new StringOutputStream(resultBuffer);
        
        Utility.doPatchString(unpatchedString, resultStream, aEnvironment, aStackTop);
        
        aEnvironment.getCurrentInput().iStatus.restoreFrom(oldStatus);
        
        setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aEnvironment.getPrecision(), resultBuffer.toString()));
    }


}//end class.



/*
%mathpiper_docs,name="PatchString",categories="Programming Procedures,Strings,Built In"
*CMD PatchString --- insert the result of evaluating code into a string 
*CORE
*CALL
	PatchString(string)

*PARMS

{string} -- a string to patch

*DESC

This procedure evaluates code that is between delimiters {? and ?} in 
a string and replaces the code (along with its delimiters) with the 
result of the evaluation

*E.G.
In> PatchString("Two plus three is {?Write(2+3);?} ");
Result: "Two plus three is 5 ";

%/mathpiper_docs

*SEE PatchLoad
*/