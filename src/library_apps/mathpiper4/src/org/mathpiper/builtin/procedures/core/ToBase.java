/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BigNumber;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;

/**
 *
 *  
 */
public class ToBase extends BuiltinProcedure
{

    private ToBase()
    {
    }

    public ToBase(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        // Get the base to convert to:
        // Evaluate car argument, and store getTopOfStackPointer in oper
        Cons oper = getArgument(aEnvironment, aStackTop, 1);
        // check that getTopOfStackPointer is a number, and that it is in fact an integer
//        LispError.check(oper.type().equals("Number"), LispError.KLispErrInvalidArg);
        BigNumber num =(BigNumber) oper.getNumber(aEnvironment.getPrecision(), aEnvironment);
        if(num == null) LispError.checkArgument(aEnvironment, aStackTop, 1);
        // check that the base is an integer between 2 and 32
        if(! num.isInteger()) LispError.checkArgument(aEnvironment, aStackTop, 1);

        // Get a short platform integer from the car argument
        int base = (int) (num.toLong());

        // Get the number to convert
        BigNumber x = org.mathpiper.lisp.Utility.getNumber(aEnvironment, aStackTop, 2);

        // convert using correct base
        String str;
        str = x.numToString(aEnvironment.getPrecision(), base);
        // Get unique string from hash table, and create an atom from it.

        setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aEnvironment.getPrecision(), Utility.toMathPiperString(aEnvironment, aStackTop, str)));
    }
}


/*
%mathpiper_docs,name="ToBase",categories="Programming Procedures,Numbers (Operations),Built In"
*CMD ToBase --- conversion of a number in decimal base to non-decimal base
*CORE
*CALL
	ToBase(base, number)

*PARMS

{base} -- integer, base to convert to/from

{number} -- integer, number to write out in a different base

{"string"} -- string representing a number in a different base

*DESC

In MathPiper, all numbers are written in decimal notation (base 10).
The two functions {FromBase}, {ToBase} convert numbers between base 10 and a different base.
Numbers in non-decimal notation are represented by strings.


*REM where is this p-adic capability? - sw
These functions use the p-adic expansion capabilities of the built-in
arbitrary precision math libraries.

Non-integer arguments are not supported.

*E.G.


Write the (decimal) number {255} in hexadecimal notation:

In> ToBase(16,255)
Result: "ff";

*SEE FromBase
%/mathpiper_docs

*SEE PAdicExpand



%mathpiper,name="ToBase",subtype="automatic_test"

Verify(ToBase(16,30),"1e");

%/mathpiper
 */
