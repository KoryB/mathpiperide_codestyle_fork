%mathpiper,def="Nth"

/* Implementation of Nth that allows extending. */
RulebaseHoldArguments("Nth",["alist", "indexOrKey"]);
RuleHoldArguments("Nth",2,10,
    And?(Equal?(Procedure?(alist),True),
            Equal?(Integer?(indexOrKey),True),
            Not?(Equal?(First(ProcedureToList(alist)),'Nth))
            ))
     MathNth(alist,indexOrKey);




RuleHoldArguments("Nth",2,14,
     And?(Equal?(String?(alist),True),List?(indexOrKey))
    )
{
  Local(result);
  result:="";
  ForEach(i,indexOrKey) { result := result + StringMidGet(i,1,alist); }
  result;
}

RuleHoldArguments("Nth",2,15,Equal?(String?(alist),True))
{
  Check(indexOrKey >? 0 &? indexOrKey <? Length(alist) + 1, "String indexes must between 1 and the length of the string (inclusive).");
  StringMidGet(indexOrKey,1,alist);
}


RuleHoldArguments("Nth",2,20,Equal?(List?(indexOrKey),True))
{
  Map('[[ii],alist[ii]],[indexOrKey]);
}

RuleHoldArguments("Nth",2,30,
   And?(
           Equal?(Generic?(alist),True),
           Equal?(GenericTypeName(alist),"Array"),
           Equal?(Integer?(indexOrKey),True)
          )
    )
{
  ArrayGet(alist,indexOrKey);
}



RuleHoldArguments("Nth", 2, 40, !? Integer?(indexOrKey) &? (Atom?(indexOrKey) |? Constant?(indexOrKey)))
{
  Local(as);
  as := Association(indexOrKey,alist);
  Decide(Not?(Equal?(as,None)),Assign(as,Nth(as,2)));
  as;
}



RuleHoldArguments("Nth", 2, 50, Type(alist) =? "Nth")
{
  MathNth(alist, indexOrKey);
}



RuleHoldArguments("Nth", 2, 100, True)
{
  Check(False, "The first argument must be indexable.");
}
%/mathpiper



%mathpiper_docs,name="Nth",categories="Programming Procedures,Lists (Operations)"
*CMD Nth --- return the $n$-th element of a list
*CORE
*CALL
        Nth(list, n)

*PARMS

{list} -- list to choose from

{n} -- index of entry to pick

*DESC

The entry with index "n" from "list" is returned. The first entry
has index 1. It is possible to pick several entries of the list by
taking "n" to be a list of indices.

More generally, {Nth} returns the n-th operand of the
expression passed as first argument.

An alternative but equivalent form of {Nth(list, n)} is
{list[n]}.

*E.G.

In> lst := [_a,_b,_c,13,19];
Result: [_a,_b,_c,13,19];

In> Nth(lst, 3);
Result: _c;

In> lst[3];
Result: _c;

In> Nth(lst, [3,4,1]);
Result: [_c,13,a];

In> Nth(_b*(_a+_c), 2);
Result: _a+_c;

*Examples

*SEE Select
%/mathpiper_docs



%mathpiper,name="Nth",subtype="in_prompts"

(lst := [_a,_b,_c,13,19]) -> [_a,_b,_c,13,19]

Nth(lst, 3) -> _c

lst[3] -> _c;

Nth(lst, [3,4,1]) -> [_c,13,_a];

Nth(_b*(_a+_c), 2) -> _a+_c;

%/mathpiper