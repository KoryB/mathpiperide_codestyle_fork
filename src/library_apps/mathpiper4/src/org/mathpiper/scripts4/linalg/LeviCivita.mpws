%mathpiper,def="LeviCivita"

/* Levi-civita symbol */
Procedure("LeviCivita",["indices"])
{
  Local(i,j,length,left,right,factor);
  length:=Length(indices);
  factor:=1;

  For (j:=length,j>?1,j--)
  {
    For(i:=1,i<?j,i++)
    {
      left:=indices[i];
      
      right:=indices[i+1];

      Decide(Equal?(left,right),
      { factor := 0 ; },
      {
        Decide(Not?(Apply("<?",[left,right])),
        {
/*
          Swap(indices,i,i+1);
*/
          indices:=Insert(Delete(indices,i),i+1,left);
          factor:= -factor;
        });
      });
    }
  }
  factor;
}

%/mathpiper

    %output,mpversion=".204",preserve="false"
      Result: True
.   %/output



%mathpiper_docs,name="LeviCivita",categories="Mathematics Procedures,Combinatorics"
*CMD LeviCivita --- totally anti-symmetric Levi-Civita symbol
*STD
*CALL
        LeviCivita(list)

*PARMS

{list} -- a list of integers 1 .. n in some order

*DESC

{LeviCivita} implements the Levi-Civita symbol. This is generally
useful for tensor calculus.  {list}  should be a list of integers,
and this procedure returns 1 if the integers are in successive order,
eg. {LeviCivita( [1,2,3,...] )}  would return 1. Swapping two elements of this
list would return -1. So, {LeviCivita( [2,1,3] )} would evaluate
to -1.

*E.G.

In> LeviCivita([1,2,3])
Result: 1;

In> LeviCivita([2,1,3])
Result: -1;

In> LeviCivita([2,2,3])
Result: 0;

*SEE PermutationsList
%/mathpiper_docs







%mathpiper,name="LeviCivita",subtype="automatic_test"

Verify(LeviCivita([1,2,3]),1);
Verify(LeviCivita([2,1,3]),-1);
Verify(LeviCivita([1,1,3]),0);

%/mathpiper

