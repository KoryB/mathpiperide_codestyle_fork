%mathpiper,def="Flatten;DoFlatten"

RulebaseHoldArguments("DoFlatten",["doflattenx"]);
UnFence("DoFlatten",1);

10 ## DoFlatten(doflattenx_)::(Type(doflattenx)=?flattenoper) <--
     Apply("Concat",MapSingle("DoFlatten",Rest(ProcedureToList(doflattenx))));
20 ## DoFlatten(doflattenx_) <-- [ doflattenx ];


Procedure("Flatten",["body", "flattenoper"])
{
  DoFlatten(body);
}

%/mathpiper

    %output,sequence="4",timestamp="2013-12-04 14:37:54.783",preserve="false"
      Result: True
.   %/output



%mathpiper_docs,name="Flatten",categories="Programming Procedures,Lists (Operations)"
*CMD Flatten --- flatten expression w.r.t. some operator
*STD
*CALL
        Flatten(expression,operator)

*PARMS

{expression} -- an expression

{operator} -- string with the contents of an infix operator.

*DESC

Flatten flattens an expression with respect to a specific
operator, converting the result into a list.
This is useful for unnesting an expression. Flatten is typically
used in simple simplification schemes.

*E.G.

In> Flatten(_a+_b*_c+_d,"+");
Result: [_a,_b*_c,_d];

In> Flatten([_a,[_b,_c],_d],"List");
Result: [-a,_b,_c,-d];

*SEE UnFlatten
%/mathpiper_docs