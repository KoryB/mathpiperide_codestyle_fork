%mathpiper,def="TruthTable"

TruthTable(booleanExpression) :=
{
    Local(resultList, variables, booleanPatterns, subexpressions, substitutionList, substitutionExpression, evaluation);
    
    resultList := [];
    
    variables := UnderscoreConstants(booleanExpression);
    
    booleanPatterns := Reverse(BooleanLists(Length(variables)));
    
    subexpressions := Subexpressions(booleanExpression);
    
    Append!(resultList, subexpressions);
    

    ForEach(booleanPattern, booleanPatterns)
    {
        substitutionList := Map("==", [variables, booleanPattern]);
        
        substitutionExpression := UnFlatten(substitutionList, "&?", True);

        evaluation := `(subexpressions Where @substitutionExpression);
        
        Append!(resultList, evaluation);
    
    }
    
    resultList;
}

%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output




%mathpiper_docs,name="TruthTable",categories="Mathematics Procedures,Propositional Logic",access="experimental"
*CMD TruthTable --- returns a list that contains a truth table for a boolean expression

*CALL
        TruthTable(booleanExpression)

*PARMS
{booleanExpression} -- a boolean expression.

*DESC
Returns a list that contains a truth table for a boolean expression. The truth table is returned as a 
list of lists.

*E.G.
In> za := TruthTable(_a &? _b)
Result: [[_a,_b,_a &? _b],[True,True,True],[True,False,False],[False,True,False],[False,False,False]]

//Substitute T and F for True and False.
In> zb := (za /: [True <- 'T, False <- 'F])
Result: [[a,b,a &? b],[T,T,T],[T,F,F],[F,T,F],[F,F,F]]

//View the table in traditional mathematics format.
In> ViewMath(zb)
Result: class javax.swing.JFrame 

In> za2 := TruthTable(0 &? _b)
Result: [[_b,0 &? _b],[True,0],[False,False]]

In> zb := (za2 /: [True <- 'T, False <- 'F])
Result: [[_b,0 &? _b],[T,0],[F,F]]


Tautology example

In> TruthTable(ObjectToMeta('((!?(!?(p &? q) &? !?(p &? r)) |? (!?(p &? (q |? r)))))))
Result: [[_p,_q,_r,_q |? _r,_p &? _q,_p &? _r, !? (_p &? _q), !? (_p &? _r),_p &? (_q |? _r), !? (_p &? (_q |? _r)), !? (_p &? _q) &?  !? (_p &? _r), !? ( !? (_p &? _q) &?  !? (_p &? _r)), !? ( !? (_p &? _q) &?  !? (_p &? _r)) |?  !? (_p &? (_q |? _r))],[True,True,True,True,True,True,False,False,True,False,False,True,True],[True,True,False,True,True,False,False,True,True,False,False,True,True],[True,False,True,True,False,True,True,False,True,False,False,True,True],[True,False,False,False,False,False,True,True,False,True,True,False,True],[False,True,True,True,False,False,True,True,False,True,True,False,True],[False,True,False,True,False,False,True,True,False,True,True,False,True],[False,False,True,True,False,False,True,True,False,True,True,False,True],[False,False,False,False,False,False,True,True,False,True,True,False,True]]


*SEE Subexpressions, BooleanList, BooleanLists

%/mathpiper_docs

    %output,preserve="false"
      
.   %/output





%mathpiper,name="TruthTable",subtype="automatic_test"

Verify(TruthTable(_a &? _b), [[_a,_b,_a &? _b],[True,True,True],[True,False,False],[False,True,False],[False,False,False]]);

%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output


