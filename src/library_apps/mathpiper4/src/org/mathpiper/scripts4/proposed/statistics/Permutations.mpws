%mathpiper,def="Permutations"

Permutations(n) :=
{
    Check(Integer?(n), "Argument must be an integer");

    Factorial(n);
}


Permutations(n, r) :=
{
    Check(Integer?(n), "Argument 1 must be an integer");
    
    Check(Integer?(r), "Argument 2 must be an integer");
    
    Factorial(n) / Factorial(n-r);
}

%/mathpiper





%mathpiper_docs,name="Permutations",categories="Mathematics Procedures,Combinatorics",access="experimental"
*CMD Permutations --- number of permutations
*STD
*CALL
    Permutations(n)
    Permutations(n, r)

*PARMS

{n} -- integer - total number of objects
{r} -- integer - number of objects chosen

*DESC

In combinatorics, this procedure is thought of as being the number of ways
to choose "r" objects out of a total of "n" objects if order is taken into account.

The single parameter version of the procedure is a convenience procedure for
calculating the number of ways to choose "n" objects out of "n" objects.

*E.G.

In> Permutations(5)
Result> 120


In> Permutations(10,3)
Result> 720

*SEE PermutationsList, Combinations, CombinationsList, LeviCivita
%/mathpiper_docs



%mathpiper,scope="nobuild",subtype="manual_test"

Permutations(4);

%/mathpiper




%mathpiper,name="Permutations",subtype="automatic_test"

    Verify(Permutations(5), 120);

    Verify(Permutations(10,3), 720);

%/mathpiper
