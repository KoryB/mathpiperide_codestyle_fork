%mathpiper,def="HighlightPattern"

HighlightPattern(expression, pattern) :=
{
    Local(list, result);
    
    list := [
        ["function",
            Lambda([trackingList,positionString,node], 
            {   
                MetaSet(node,"HighlightColor","ORANGE");            
            })
        ]
    ];
    
    result := TreeProcess(expression, pattern, list);
}

%/mathpiper





%mathpiper_docs,name="HighlightPattern",categories="Programming Procedures,Expression Trees"
*CMD HighlightPattern --- highlights all subtrees of an expression tree that match the given pattern

*CALL
    HighlightPattern(expression, pattern)

*PARMS

{expression} -- expression tree to be highlighted

{pattern} -- pattern to match against subtrees

*DESC

Highlights all subtrees of an expression tree that
match the given pattern by setting metadata on the
subtrees. Highlighting metadata is only applied to
the dominant node because the tree viewer will
highlight a node's subtree if its dominant node is
highlighted.

*E.G.
In> result := HighlightPattern('(1+2-3), a_Integer?)
Result: (1 + 2) - 3

In> MetaEntries(result[1][1])
Result: ["HighlightColor":"ORANGE"]

*SEE Mark, MarkAll
%/mathpiper_docs





%mathpiper,name="HighlightPattern",subtype="automatic_test"

{
    Local(result);
    
    result := HighlightPattern('(1+2-3), a_Integer?);
    
    Verify(MetaGet(result[1][1], "HighlightColor"), "ORANGE");
    
    Verify(MetaGet(result[1][2], "HighlightColor"), "ORANGE");
    
    Verify(MetaGet(result[2], "HighlightColor"), "ORANGE");
}

%/mathpiper
