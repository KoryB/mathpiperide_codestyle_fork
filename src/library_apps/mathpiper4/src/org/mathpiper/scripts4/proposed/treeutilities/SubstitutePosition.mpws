%mathpiper,def="SubstitutePosition"

SubstitutePosition(expression, position, pattern, replacement) :=
{
    If(Verbose?())
    {
        Echo("SubstitutePosition");
    }
    
    SubstitutePosition(expression, position, pattern, replacement, []);
}



SubstitutePosition(expression, position, pattern, replacement, variables) :=
{
    Local(list, result);
    
    list := [
        ["function",
            Lambda([trackingList,positionString,node], 
            {    
                ForEach(constant, UnderscoreConstants(pattern))
                {
                    constant := ToAtom(StringSubstring(ToString(constant), 1, StringIndexOf(ToString(constant), "_")));
                    
                    If(constant !=? _ &? {Local(variable); variable := MetaToObject(constant); `Assigned?(@variable);})
                    {
                        variables[constant] := Eval(MetaToObject(constant));
                    }
                }
                
                Eval(replacement);
            })
        ]
    ];
    
    result := TreeProcess(expression, pattern, list, Position:position);
}

%/mathpiper





%mathpiper_docs,name="SubstitutePosition",categories="Programming Procedures,Expression Trees"
*CMD SubstitutePosition --- replace a subexpression at a given position if it matches the given pattern

*CALL
    SubstitutePosition(expression, position, pattern, replacement)
    SubstitutePosition(expression, position, pattern, replacement, variables)

*PARMS

{expression} -- an expression

{position} -- an expression tree position in string form

{pattern} -- a rule pattern (the head of a rule)

{replacement} -- replacement expression (the body of a rule)

{variables} -- an empty association list that matched variable values will be placed into

*DESC

Replace a subexpression at a given position if it matches the given pattern. If an empty
association list is passed in as the 5th argument, the values of matched variables
will be placed into it. The key for each matched variable is the variable
name followed by an underscore. So the key for variable 'q' would be 'q_'.

*E.G.
In> SubstitutePosition('(2 + (_b*_c) / _b - 3), "1,1", q_Integer?, '(q*2))
Result: (4 + (_b*_c)/_b) - 3




/%mathpiper

patternVariables := [];

result := SubstitutePosition('(2 + (_b*_c) / _b - 3), "1,1", q_Integer?, '(q*2), patternVariables);

q := patternVariables[q_];

Echo("result: " + result + Nl() + "q: " + q);

/%/mathpiper

    /%output,mpversion=".231",preserve="false"
      Result: True
      
      Side Effects:
      result: (4 + (_b*_c)/_b) - 3
      q: 2
      
.   /%/output

*SEE PositionGet
%/mathpiper_docs





%mathpiper,name="SubstitutePosition",subtype="automatic_test"

Verify(SubstitutePosition('(2 + (_b*_c) / _b - 3), "1,1", q_Integer?, '(q*2)), (4 + (_b*_c)/_b) - 3);

Verify(
{
    Local(patternVariables);
    patternVariables := [];
    [
    SubstitutePosition('(2 + (_b*_c) / _b - 3), "1,1", q_Integer?, '(q*2), patternVariables),
    patternVariables[q_]
    ];
},
[(4 + (_b*_c)/_b) - 3, 2]);

%/mathpiper

