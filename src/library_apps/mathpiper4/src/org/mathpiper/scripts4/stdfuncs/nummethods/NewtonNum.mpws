%mathpiper,def="NewtonNum"

/// coded by Serge Winitzki. See essays documentation for algorithms.

//////////////////////////////////////////////////
/// Numerical method: Newton-like superconvergent iteration
//////////////////////////////////////////////////

// Newton's method, generalized, with precision control and diagnostics

/// auxiliary utility: compute the number of common decimal digits of x and y (using relative precision)
Commondigits(x,y) :=
{
        Local(diff);
        diff := Abs(x-y);
        Decide(
                diff=?0,
                Infinity,
                // use approximation Ln(2)/Ln(10) >? 351/1166
                Quotient(IntLog(FloorN(DivideN(Maximum(Abs(x), Abs(y)), diff)), 2)*351, 1166)
        );         // this many decimal digits in common
}

///interface
NewtonNum(func_, x0_) <-- NewtonNum(func, x0, 5);        // default prec0
NewtonNum(func_, x0_, prec0_) <-- NewtonNum(func, x0, prec0, 2);

// func is the procedure to iterate, i.e. x = func(x).
// prec0 is the initial precision necessary to get convergence started.
// order is the order of convergence of the given sequence (e.g. 2 or 3).
// x0 must be close enough so that x1 has a few common digits with x0 after at most 5 iterations.
NewtonNum(func_, xinit_, prec0_, order_) <--
{
        Check(prec0>=?4, "NewtonNum: Error: initial precision must be at least 4");
        Check(Integer?(order) &? order>?1, "NewtonNum: Error: convergence order must be an integer and at least 2");
        Local(x0, x1, prec, exactdigits, intpart, initialtries);
  NM({
    x0 := xinit;
    prec := BuiltinPrecisionGet();
    intpart := IntLog(Ceil(Abs(x0)), 10);        // how many extra digits for numbers like 100.2223
    // intpart must be set to 0 if we have true floating-point semantics of BuiltinPrecisionSet()
    BuiltinPrecisionSet(2+prec0-intpart);        // 2 guard digits
    x1 := (Apply(func, [x0]));        // let's run one more iteration by hand
    // first, we get prec0 exact digits
    exactdigits := 0;
    initialtries := 5;        // stop the loop the the initial value is not good
    While(exactdigits*order <? prec0 &? initialtries>?0)
    {
      initialtries--;
      x0 := x1;
      x1 := (Apply(func, [x0]));
      exactdigits := Commondigits(x0, x1);
  //                Decide(Verbose?(), Echo("NewtonNum: Info: got", exactdigits, "exact digits at prec. ", BuiltinPrecisionGet()));
    }
    // need to check that the initial precision is achieved
    Decide(
      Assert("value", ["NewtonNum: Error: need a more accurate initial value than", xinit])
        exactdigits >=? 1,
    {
    exactdigits :=Minimum(exactdigits, prec0+2);
    // run until get prec/order exact digits
    intpart := IntLog(Ceil(Abs(x1)), 10);        // how many extra digits for numbers like 100.2223
    While(exactdigits*order <=? prec)
    {
      exactdigits := exactdigits*order;
      BuiltinPrecisionSet(2+Minimum(exactdigits, Quotient(prec,order)+1)-intpart);
      x0 := x1;
      x1 := (Apply(func, [x0]));
  //                Decide(Verbose?(), Echo("NewtonNum: Info: got", Commondigits(x0, x1), "exact digits at prec. ", BuiltinPrecisionGet()));
    }
    // last iteration by hand
    BuiltinPrecisionSet(2+prec);
    x1 := RoundTo( (Apply(func, [x1])), prec);
    },
    // did not get a good initial value, so return what we were given
    x1 := xinit
    );
    BuiltinPrecisionSet(prec);
  });
        x1;
}


/*
example: logarithm function using cubically convergent Newton iteration for
Exp(x/2)-a*Exp(-x/2)=0:

x := x - 2 * (Exp(x)-a) / (Exp(x)+a)

LN(x_Number?)_(x>?1 ) <--
        LocalSymbols(y)
[
// initial guess is obtained as Ln(x^2)/Ln(2) * (Ln(2)/2)
        NewtonNum([[y],4*x/(Exp(y)+x)-2+y], NM(794/2291*IntLog(Floor(x*x),2),5), 10, 3);
];
*/

%/mathpiper



%mathpiper_docs,name="NewtonNum",categories="Mathematics Procedures,Solvers (Numeric)"
*CMD NewtonNum --- low-level optimized Newton's iterations
*STD
*CALL
        NewtonNum(func, x0, prec0, order)
        NewtonNum(func, x0, prec0)
        NewtonNum(func, x0)

*PARMS

{func} -- a procedure specifying the iteration sequence

{x0} -- initial value (must be close enough to the root)

{prec0} -- initial precision (at least 4, default 5)

{order} -- convergence order (typically 2 or 3, default 2)

*DESC

This procedure is an optimized interface for computing Newton's
iteration sequences for numerical solution of equations in arbitrary precision.

{NewtonNum} will iterate the given function starting from the initial
value, until the sequence converges within current precision.
Initially, up to 5 iterations at the initial precision {prec0} is
performed (the low precision is set for speed). The initial value {x0}
must be close enough to the root so that the initial iterations
converge. If the sequence does not produce even a single correct digit
of the root after these initial iterations, an error message is
printed. The default value of the initial precision is 5.

The {order} parameter should give the convergence order of the scheme.
Normally, Newton iteration converges quadratically (so the default
value is {order}=2) but some schemes converge faster and you can speed
up this procedure by specifying the correct order. (Caution: if you give
{order}=3 but the sequence is actually quadratic, the result will be
silently incorrect. It is safe to use {order}=2.)

*REM
The verbose option {V} can be used to monitor the convergence. The
achieved exact digits should roughly form a geometric progression.

*E.G.
In> BuiltinPrecisionSet(20)
Result: True;

In> NewtonNum([[x], x+Sin(x)], 3, 5, 3)
Result: 3.14159265358979323846;

%/mathpiper_docs

*SEE Newton




%mathpiper,name="NewtonNum",subtype="automatic_test"

NumericEqual(NewtonNum('[[x], x+SinN(x)], 3, 5, 3), 3.14159265358979, 20);

%/mathpiper