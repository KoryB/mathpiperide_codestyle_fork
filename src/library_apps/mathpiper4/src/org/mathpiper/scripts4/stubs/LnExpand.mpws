%mathpiper,def="LnExpand"

////////////////////// Log rules stuff //////////////////////

// LnExpand
1 ## LnExpand(Ln(x_Integer?))
                            <-- Add(Map([[n,m],m*Ln(n)],Transpose(Factors(x))));
1 ## LnExpand(Ln(a_*b_))     <-- LnExpand(Ln(a))+LnExpand(Ln(b));
1 ## LnExpand(Ln(a_/b_))     <-- LnExpand(Ln(a))-LnExpand(Ln(b));
1 ## LnExpand(Ln(a_^n_))     <-- LnExpand(Ln(a))*n;
2 ## LnExpand(a_)            <-- a;

%/mathpiper



%mathpiper_docs,name="LnExpand",categories="Mathematics Procedures,Expression Simplification"
*CMD LnExpand --- expand a logarithmic expression using standard logarithm rules
*STD
*CALL
        LnExpand(expr)

*PARMS

{expr} -- the logarithm of an expression

*DESC

{LnExpand} takes an expression of the form $Ln(expr)$, and applies logarithm
rules to expand this into multiple {Ln} expressions where possible.  An
expression like $Ln(a*b^n)$ would be expanded to $Ln(a)+n*Ln(b)$.

If the logarithm of an integer is discovered, it is factorised using {Factors}
and expanded as though {LnExpand} had been given the factorised form.  So 
$Ln(18)$ goes to $Ln(x)+2*Ln(3)$.

*E.G.
In> LnExpand(Ln(a*b^n))
Result: Ln(a)+Ln(b)*n

In> LnExpand(Ln(a^m/b^n))
Result: Ln(a)*m-Ln(b)*n

In> LnExpand(Ln(60))
Result: 2*Ln(2)+Ln(3)+Ln(5)

In> LnExpand(Ln(60/25))
Result: 2*Ln(2)+Ln(3)-Ln(5)

*SEE Ln, LnCombine
%/mathpiper_docs

*SEE Factors