
package org.mathpiper.ui.gui;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.io.InputStream;
import javax.swing.JTextArea;


public class JTextAreaNoCopyPaste extends JTextArea
{
    
    public JTextAreaNoCopyPaste() throws Exception
    {
        super();
        setFont(newFont(14));
    }
        
    public JTextAreaNoCopyPaste(String text, int rows, int columns) throws Exception
    {
        super(text, rows, columns);
        setFont(newFont(14));
    }
    
    public void setFontSize(int fontSize) throws Exception
    {
        setFont(newFont(fontSize));
    }
    
    private Font newFont(int fontSize) throws Exception
    {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("org/mathpiper/DejaVuSansMono.ttf");
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, inputStream));
        Font font = new Font("DejaVu Sans", Font.PLAIN, fontSize);
        
        return font;
    }
    
    public void copy()
    {
    }
    
    public void paste()
    {
    }
}

