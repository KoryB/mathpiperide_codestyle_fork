package org.mathpiper.ui.gui.applications.circuitpiper.view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */

public final class ButtonPanel extends JPanel {

    private WrapLayout wrapLayout;
    static final String[] componentNames = {"Resistor", "Capacitor", "Inductor", "Voltage Source", "Current Source",
        "Wire", "Voltmeter", "Ammeter", "Ohmmeter", "Capacitance Meter", "Inductance Meter", "Switch", "Current Integrator", "Voltage Integrator", "AC Voltage Source", "AC Current Source", "VCVS", "VCCS", "CCVS", "CCCS", "Block", "Transistor NPN", "Transistor PNP", "Transistor JFET N", "Transistor JFET P", "Logical Package"};

    public ButtonPanel(final CircuitPanel parentCircuitEnginePanel) {
        super();
        wrapLayout = new WrapLayout();
        this.setLayout(wrapLayout);
        this.add(new JLabel("Component: "));
        JComboBox componentList = new JComboBox(componentNames);
        componentList.setMaximumRowCount(25);
        this.add(componentList);
        componentList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox) e.getSource();
                String componentName = (String) cb.getSelectedItem();
                parentCircuitEnginePanel.setSelectedComponent(componentName);
                parentCircuitEnginePanel.setIsDrawing(false);
                parentCircuitEnginePanel.setIsMovingPoint(false);
                parentCircuitEnginePanel.setHintStarting();
                parentCircuitEnginePanel.repaint();
                parentCircuitEnginePanel.revalidate();
            }
        });
        componentList.setSelectedIndex(0);
        parentCircuitEnginePanel.setSelectedComponent("Resistor");

        JToggleButton runButton = new JToggleButton("Run");
        runButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JToggleButton toggleButton = (JToggleButton) e.getSource();
                if(toggleButton.isSelected())
                {
                    parentCircuitEnginePanel.isRunning = true;
                }
                else
                {
                    parentCircuitEnginePanel.isRunning = false;
                }
            }
        });
        this.add(runButton);
        
        JButton dumpButton = new JButton("Dump");
        dumpButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try
                {
                    JTextArea textArea = new JTextArea(20, 30);
                    textArea.setFont(new Font("monospaced", Font.PLAIN, 12));
                    textArea.append(parentCircuitEnginePanel.circuit.dumpMatrix());
                    
                    JFrame frame = new JFrame();
                    Container contentPane = frame.getContentPane();
                    
                    contentPane.add(new JScrollPane(textArea));
                    
                    frame.setResizable(true);
                    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    frame.pack();
                    //frame.setSize(400,300);
                    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                    int height = screenSize.height;
                    int width = screenSize.width;
                    //frame.setSize(width/2, height/2);
                    frame.setLocationRelativeTo( null );
                    frame.setVisible(true);
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }
        });
        this.add(dumpButton);
        
                

        // playButton.doClick(); todo:tk.
        
        /*
        JCheckBox probeCheckBox = new JCheckBox("Probe");
        //probeCheckBox.setMnemonic(KeyEvent.VK_C); 
        probeCheckBox.setSelected(false);
        probeCheckBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED)
                {
                    parentCircuitEnginePanel.isProbe = true;
                }
                else
                {
                    parentCircuitEnginePanel.isProbe = false;
                }
            }
        });
        this.add(probeCheckBox);
        */
        
        JCheckBox showGridCheckBox = new JCheckBox("Grid");
        //probeCheckBox.setMnemonic(KeyEvent.VK_C); 
        showGridCheckBox.setSelected(true);
        showGridCheckBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED)
                {
                    parentCircuitEnginePanel.drawingPanel.isDrawGrid = true;
                    parentCircuitEnginePanel.repaint();
                }
                else
                {
                    parentCircuitEnginePanel.drawingPanel.isDrawGrid = false;
                    parentCircuitEnginePanel.repaint();
                }
            }
        });
        parentCircuitEnginePanel.showGridCheckBox = showGridCheckBox;
        this.add(showGridCheckBox);

        
        JCheckBox showWiresCheckBox = new JCheckBox("Wires");
        //probeCheckBox.setMnemonic(KeyEvent.VK_C); 
        showWiresCheckBox.setSelected(false);
        showWiresCheckBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED)
                {
                    parentCircuitEnginePanel.drawingPanel.isDrawWireLabels = true;
                    parentCircuitEnginePanel.repaint();
                }
                else
                {
                    parentCircuitEnginePanel.drawingPanel.isDrawWireLabels = false;
                    parentCircuitEnginePanel.repaint();
                }
            }
        });
        this.add(showWiresCheckBox);
        
        JCheckBox showTerminalsCheckBox = new JCheckBox("Terminals");
        //probeCheckBox.setMnemonic(KeyEvent.VK_C); 
        showTerminalsCheckBox.setSelected(false);
        showTerminalsCheckBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED)
                {
                    parentCircuitEnginePanel.drawingPanel.isDrawTerminalLabels = true;
                    parentCircuitEnginePanel.repaint();
                }
                else
                {
                    parentCircuitEnginePanel.drawingPanel.isDrawTerminalLabels = false;
                    parentCircuitEnginePanel.repaint();
                }
            }
        });
        this.add(showTerminalsCheckBox);
  
    
        /*
        JButton moveLeftButton = new JButton("Left");
        moveLeftButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    for (Terminal terminal : parentCircuitEnginePanel.circuit.myTerminals.values()) {
                       terminal.setX(terminal.getX() - parentCircuitEnginePanel.xDistanceBetweenTerminalsPixels);
                    }               
                    parentCircuitEnginePanel.repaint();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        this.add(moveLeftButton);
        
        
        JButton moveRightButton = new JButton("Right");
        moveRightButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    for (Terminal terminal : parentCircuitEnginePanel.circuit.myTerminals.values()) {
                       terminal.setX(terminal.getX() + parentCircuitEnginePanel.xDistanceBetweenTerminalsPixels);
                    }               
                    parentCircuitEnginePanel.repaint();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        this.add(moveRightButton);
        
        
        JButton moveUpButton = new JButton("Up");
        moveUpButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    for (Terminal terminal : parentCircuitEnginePanel.circuit.myTerminals.values()) {
                       terminal.setY(terminal.getY() - parentCircuitEnginePanel.yDistanceBetweenTerminalsPixels);
                    }               
                    parentCircuitEnginePanel.repaint();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        this.add(moveUpButton);
        
        
        JButton moveDownButton = new JButton("Down");
        moveDownButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    for (Terminal terminal : parentCircuitEnginePanel.circuit.myTerminals.values()) {
                       terminal.setY(terminal.getY() + parentCircuitEnginePanel.yDistanceBetweenTerminalsPixels);
                    }               
                    parentCircuitEnginePanel.repaint();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        this.add(moveDownButton);
        */
        
  }
}
