package org.mathpiper.ui.gui.applications.voscilloscope.oscilloscope;

import org.mathpiper.ui.gui.applications.voscilloscope.simulator.SuspendThread;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * <p>Title: Virtual Oscilloscope.</p>
 * <p>Description: A Oscilloscope simulator</p>
 * <p>Copyright (C) 2003 José Manuel Gómez Soriano</p>
 * <h2>License</h2>
 * <p>
 This file is part of Virtual Oscilloscope.

 Virtual Oscilloscope is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Virtual Oscilloscope is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Virtual Oscilloscope; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 * </p>
 */

public class SignalClient extends SuspendThread
{

    private byte data[];
    private boolean signal;
    static final int ADPORT = 5454;
    private SignalManager signals = null;

    public SignalClient(SignalManager signals)
    {
        this.signals = signals;
    }

    public synchronized void eraseSignal()
    {
        signal = false;
        data = null;
    }

    public synchronized boolean hasSignal()
    {
        return signal;
    }

    public synchronized int getNSamples()
    {
        return signal ? data.length : 0;
    }

    public synchronized byte[] getData()
    {
        return data;
    }

    public synchronized void init()
    {
        eraseSignal();
    }

    public void run()
    {
        try
        {
            ServerSocket ss = new ServerSocket(5454);
            ss.setSoTimeout(0);
            do
            {
                System.out.println("Waiting for connections");
                Socket s = ss.accept();
                System.out.println("Connection accepted");
                BufferedInputStream in = new BufferedInputStream(s.getInputStream());
                ByteArrayOutputStream out = new ByteArrayOutputStream();

                synchronized(this)
                {
                    eraseSignal();
                    do
                    {
                        byte data[] = new byte[in.available()];
                        System.out.println(data.length);
                        if(in.read(data) == -1) break;

                        out.write(data);

                    }
                    while(true);
                    data = out.toByteArray();
System.out.println(data.length);
                    setSignal(data);
                    signal = true;
                }
                in.close();
                s.close();
            } while(true);
        }
        catch(IOException e)
        {
            System.out.println("Connection error");
        }
    }

    private void setSignal(byte data[])
    {
        if(signals==null){
            System.out.println("[Error] The variable \"signals\" is null!");
            return;
        }
        if(!signals.isSuspend())
        {
            signals.suspender();
        }
        signals.channel1.conectaSignal(data);
        signals.rewind();
        if(signals.isSuspend())
        {
            signals.reanudar();
        }
    }
}

