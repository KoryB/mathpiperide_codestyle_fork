package org.mathpiper.ui.gui.worksheets.symbolboxes;

public class Fraction extends CompoundExpression {

    private int iDashheight = 0;

    private SymbolBox iNumerator;

    private SymbolBox iDenominator;

    public Fraction(SymbolBox aNumerator, SymbolBox aDenominator) {

        iNumerator = aNumerator;

        iDenominator = aDenominator;
    }

    public void calculatePositions(ScaledGraphics sg, int aSize, Position aPosition) {
        iScale = aSize;
        iPosition = aPosition;
        iDashheight = ScaledGraphics.fontForSize(iScale);

        if (iDimension == null) {
            iNumerator.calculatePositions(sg, aSize, null);
            iDenominator.calculatePositions(sg, aSize, null);

            Dimensions numeratorDimension = iNumerator.getDimension();
            Dimensions denominatorDimension = iDenominator.getDimension();
            double width = numeratorDimension.width;

            if (width < denominatorDimension.width) {
                width = denominatorDimension.width;
            }

            iDimension = new Dimensions(width, numeratorDimension.height + denominatorDimension.height + iDashheight);
            iAscent = numeratorDimension.height + iDashheight;
        }

        if (aPosition != null) {

            Dimensions numeratorDimension = iNumerator.getDimension();
            Dimensions denominatorDimension = iDenominator.getDimension();
            double numeratorY = aPosition.y - numeratorDimension.height + iNumerator.getCalculatedAscent() - iDashheight;
            double denominatorY = aPosition.y + iDenominator.getCalculatedAscent();
            iNumerator.calculatePositions(sg, aSize, new Position( (aPosition.x + (iDimension.width - numeratorDimension.width) / 2), numeratorY));
            iDenominator.calculatePositions(sg, aSize, new Position( (aPosition.x + (iDimension.width - denominatorDimension.width) / 2), denominatorY));
        }
    }

    public void render(ScaledGraphics sg) {

        if(drawBoundingBox) drawBoundingBox(sg);

        iNumerator.render(sg);
        
        iDenominator.render(sg);

        Dimensions ndim = iNumerator.getDimension();
        Dimensions ddim = iDenominator.getDimension();
        double width = ndim.width;

        if (width < ddim.width) {
            width = ddim.width;
        }

        sg.setLineWidth(1);
        sg.drawLine(iPosition.x, iPosition.y - iDashheight / 2 + 2, iPosition.x + width, iPosition.y - iDashheight / 2 + 2);
    }



    public SymbolBox[] getChildren()
    {
        return new SymbolBox[] {this.iNumerator, this.iDenominator};
    }//end method.





    public String toString()
    {
        String returnString = "<Fraction>";
        return returnString;
    }//end method.

}//end class
