%mathpiper,def="NumericProcedure;NumericProcedureNumberize;NumericProcedureNumberize"

/* NumericProcedure("newfunc", "oldfunc" [arglist]) will define a wrapper function
around  "oldfunc", called "newfunc", which will return "oldfunc(arglist)"
only when all arguments are numbers and will return unevaluated
"newfunc(arglist)" otherwise. */
LocalSymbols(NumericProcedureNumberize)
{
NumericProcedure(newname_String?, oldname_String?, arglist_List?) <-- {
        RulebaseEvaluateArguments(newname, arglist);
        RuleEvaluateArguments(newname, Length(arglist), 0,        // check whether all args are numeric
                ListToFunction([NumericList?, arglist])
        )

                /* this is the rule defined for the new function.
                // this expression should evaluate to the body of the rule.
                // the body looks like this:
                // NumericProcedureNumberize(oldname(arglist))
                */
                        NumericProcedureNumberize(ListToFunction([ToAtom("@"), oldname, arglist]));
                        // cannot use bare '@' b/c get a syntax error

};

// this function is local to NumericProcedure.
// special handling for numerical errors: return Undefined unless given a number.
10 # NumericProcedureNumberize(x_Number?) <-- x;
20 # NumericProcedureNumberize(x_Atom?) <-- Undefined;
// do nothing unless given an atom

};        // LocalSymbols()

%/mathpiper



%mathpiper_docs,name="NumericProcedure",categories="Programming Functions;Functional Operators"
*CMD NumericProcedure --- make wrapper for numeric functions
*STD
*CALL
        NumericProcedure("newname","funcname", [arglist])

*PARMS
{"newname"} -- name of new function

{"funcname"} -- name of an existing function

{arglist} -- symbolic list of arguments

*DESC
This function will define a function named "newname"
with the same arguments as an existing function named "funcname". The new function 
will evaluate and return the expression "funcname(arglist)" only when
all items in the argument list {arglist} are numbers, and return unevaluated otherwise.

This can be useful when plotting functions defined through other MathPiper routines 
that cannot return unevaluated.

If the numerical calculation does not return a number (for example,
it might return the atom {nan}, "not a number", for some arguments),
then the new function will return {Undefined}.

This operator can help the user to program in the style of functional programming languages such as Miranda or Haskell.


*E.G. notest

In> f(x) := NM(Sin(x));
Result: True;

In> NumericProcedure("f1", "f", [x]);
Result: True;

In> f1(a);
Result: f1(a);

In> f1(0);
Result: 0;
Suppose we need to define a complicated function [t(x)] which cannot be evaluated unless [x] is a number:

In> t(x) := Decide(x<=?0.5, 2*x, 2*(1-x));
Result: True;

In> t(0.2);
Result: 0.4;

In> t(x);
        In function "If" :
        bad argument number 1 (counting from 1)
        CommandLine(1) : Invalid argument
Then, we can use [NumericProcedure()] to define a wrapper [t1(x)] around [t(x)] which will not try to evaluate [t(x)] unless [x] is a number.

In> NumericProcedure("t1", "t", [x])
Result: True;

In> t1(x);
Result: t1(x);

In> t1(0.2);
Result: 0.4;
Now we can plot the function.

In> Plot2D(t1(x), -0.1: 1.1)
Result: True;

*SEE RuleEvaluateArguments
%/mathpiper_docs





%mathpiper,name="NumericProcedure",subtype="automatic_test"

BuiltinPrecisionSet(10);
Retract("f",1);
Retract("f1",1);
f(x) := NM(Abs(1/x-1));
Verify(f(0), Infinity);
NumericEqual(RoundToN(f(3),BuiltinPrecisionGet()), 0.6666666667,BuiltinPrecisionGet());
NumericProcedure("f1", "f", [x]);
Verify(f1(0), Undefined);
NumericEqual(RoundToN(f1(3),BuiltinPrecisionGet()), 0.6666666667,BuiltinPrecisionGet());
Retract("f",1);
Retract("f1",1);
%/mathpiper