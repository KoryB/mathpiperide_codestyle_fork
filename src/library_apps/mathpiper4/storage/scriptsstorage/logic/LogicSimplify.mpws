%mathpiper,def="LogicSimplify"

 // (expression, level=1..3

// Some shortcuts to match prev interface

10 # LogicSimplify(_proposition, _level)_(level<?2)  <-- CNF(proposition);

20 # LogicSimplify(_proposition, _level) <--
{
  Local(cnf, list, clauses);
  Check(level >? 1, "Argument", "Wrong level");
  // First get the CNF version of the proposition
  Assign(cnf, CNF(proposition));

  Decide(level <=? 1, cnf, {
    Assign(list, Flatten(cnf, "And?"));
    Assign(clauses, []);
    ForEach(clause, list)
    {
      Local(newclause);
      //newclause := BubbleSort(LogicRemoveTautologies(Flatten(clause, "Or?")), LessThan);
      Assign(newclause, LogicRemoveTautologies(Flatten(clause, "Or?")));
      Decide(newclause !=? [True], Append!(clauses, newclause));
    };

    /*
        Note that we sort each of the clauses so that they look the same,
        i.e. if we have (A And? B) And? ( B And? A), only the first one will
        persist.
    */
    Assign(clauses, RemoveDuplicates(clauses));

    Decide(Equal?(level, 3) And? (Length(clauses) !=? 0), {
        Assign(clauses, DoUnitSubsumptionAndResolution(clauses));
        Assign(clauses, LogicCombine(clauses));
    });

    Assign(clauses, RemoveDuplicates(clauses));

    Decide(Equal?(Length(clauses), 0), True, {
        /* assemble the result back into a boolean expression */
        Local(result);
        Assign(result, True);
        ForEach(item,clauses)
        {
            Assign(result, result And? UnFlatten(item, "Or?", False));
        };

        result;
    });
  });
};

%/mathpiper