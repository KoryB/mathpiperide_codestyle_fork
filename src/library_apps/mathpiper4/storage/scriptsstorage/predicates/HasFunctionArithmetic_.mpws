%mathpiper,def="HasFunctionArithmetic?"

/// Analyse arithmetic expressions

HasFunctionArithmetic?(expr, atom) := HasFunctionSome?(expr, atom, [ToAtom("+"), ToAtom("-"), *, /]);

%/mathpiper



%mathpiper_docs,name="HasFunctionArithmetic?",categories="Programming Functions;Predicates"
*CMD HasFunctionArithmetic? --- check for expression containing a function
*STD
*CALL
        HasFunctionArithmetic?(expr, func)

*PARMS

{expr} -- an expression

{func} -- a function atom to be found

*DESC

{HasFunctionArithmetic?} is defined through {HasFunctionSome?} to look 
only at arithmetic operations {+}, {-}, {*}, {/}.

Note that since the operators "{+}" and "{-}" are prefix as well as infix 
operators, it is currently required to use {ToAtom("+")} to obtain the 
unevaluated atom "{+}".

*E.G.

In> HasFunctionArithmetic?(x+y*Cos(Ln(x)/x), Cos)
Result: True;

In> HasFunctionArithmetic?(x+y*Cos(Ln(x)/x), Ln)
Result: False;

*SEE HasFunction?, HasFunctionSome?, FuncList, VarList, HasExpression?
%/mathpiper_docs






%mathpiper,name="HasFunctionArithmetic?",subtype="automatic_test"

Verify(HasFunctionArithmetic?(a*b+1,ToAtom("+")),True);
Verify(HasFunctionArithmetic?(a+Sin(b*c),*),False);
Verify(HasFunctionArithmetic?(a+Sin(b*c),Sin),True);

RulebaseHoldArguments("f",[a]);
Verify(HasFunctionArithmetic?(a*b+f([b,c]),List),False);
Retract("f",1);

%/mathpiper