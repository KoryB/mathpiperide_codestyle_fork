%mathpiper,def="Orthogonal?"

Orthogonal?(A_Matrix?) <-- (Transpose(A)*A=?Identity(Length(A)));

%/mathpiper



%mathpiper_docs,name="Orthogonal?",categories="Programming Functions;Predicates"
*CMD Orthogonal? --- test for an orthogonal matrix
*STD
*CALL
        Orthogonal?(A)

*PARMS

{A} -- square matrix

*DESC

{Orthogonal?(A)} returns {True} if {A} is orthogonal and {False} 
otherwise. $A$ is orthogonal iff $A$*Transpose($A$) = Identity, or
equivalently Inverse($A$) = Transpose($A$).

*E.G.

In> A := [[1,2,2],[2,1,-2],[-2,2,-1]];
Result: [[1,2,2],[2,1,-2],[-2,2,-1]];

In> UnparseMath2D(A/3)

        /                      \
        | / 1 \  / 2 \ / 2 \   |
        | | - |  | - | | - |   |
        | \ 3 /  \ 3 / \ 3 /   |
        |                      |
        | / 2 \  / 1 \ / -2 \  |
        | | - |  | - | | -- |  |
        | \ 3 /  \ 3 / \ 3  /  |
        |                      |
        | / -2 \ / 2 \ / -1 \  |
        | | -- | | - | | -- |  |
        | \ 3  / \ 3 / \ 3  /  |
        \                      /
Result: True;

In> Orthogonal?(A/3)
Result: True;
%/mathpiper_docs