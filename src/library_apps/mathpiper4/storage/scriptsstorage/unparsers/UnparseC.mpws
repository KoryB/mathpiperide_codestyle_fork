%mathpiper,def="UnparseC;UnparseCDoublePrecisionNumber;UnparseCArgs;UnparseCStatement;UnparseCMathFunctions"

/* UnparseC: convert MathPiper objects to C/C++ code. */

/* version 0.3 */

/* Changelog
        0.1        UnparseC() derived from UnparseLatex() v0.4. Have basic functionality. Do not allow list manipulation, unevaluated derivatives, set operations, limits, integrals, Infinity, explicit matrices. Complex numbers and expressions are handled just like real ones. Indexed symbols are assumed to be arrays and handled literally. No declarations or prototypes are supplied. Function definitions are not handled. Sum() is left as is (can be defined as a C function).
        0.2 Fix for extra parens in Sin() and other functions; fixes for Exp(), Abs() and inverse trig functions
        0.3 Fix for indexed expressions: support a[2][3][4]
        0.3.1 Fix for UnparseC(integer): add a decimal point
        0.4 Support While()[]. Added CUnparsable?. Use Concat() instead of Union() on lists.
        0.4.1 Support False, True
  0.4.2 Changed it so that integers are not coerced to floats any more automatically (one can coerce integers to floats manually nowadays by adding a decimal point to the string representation, eg. 1. instead of 1).
*/

/* To do:
        0. Find and fix bugs.
        1. Chop strings that are longer than 80 chars?
        2. Optimization of C code?
*/

RulebaseHoldArguments("UnparseC",[expression]);
RulebaseHoldArguments("UnparseC",[expression, precedence]);

Function ("UnparseCBracketIf", [predicate, string])
{
        Check(Boolean?(predicate) And? String?(string), "Argument", "UnparseC internal error: non-boolean and/or non-string argument of UnparseCBracketIf");
        Decide(predicate, ConcatStrings("( ", string, ") "), string);
};

UnparseCDoublePrecisionNumber(x_Number?) <--
{
    Local(i,n,s,f);
    s := ToString(x);
    n := Length(s);
    f := False;
    For(i := 1, i <=? n, i++)
    {
        Decide(s[i] =? "e" Or? s[i] =? ".", f := True);
    };
    Decide(f, s, s ~ ".");
};

/* Proceed just like UnparseLatex()
*/

// UnparseCMaxPrec should perhaps only be used from within this file, it is thus not in the .def file.
UnparseCMaxPrec() := 60000;         /* This precedence will never be bracketed. It is equal to KMaxPrec */

100 # UnparseC(_x) <-- UnparseC(x, UnparseCMaxPrec());

/* Replace numbers and variables -- never bracketed except explicitly */
110 # UnparseC(x_Integer?, _p) <-- ToString(x);
111 # UnparseC(x_Zero?, _p) <-- "0.";
112 # UnparseC(x_Number?, _p) <-- UnparseCDoublePrecisionNumber(x);
/* Variables are left as is, except some special ones */
190 # UnparseC(False, _p) <-- "false";
190 # UnparseC(True, _p) <-- "true";
200 # UnparseC(x_Atom?, _p) <-- ToString(x);

/* Strings must be quoted but not bracketed */
100 # UnparseC(x_String?, _p) <-- ConcatStrings("\"", x, "\"");

/* Replace operations */

/* arithmetic */

/* addition, subtraction, multiplication, all comparison and logical operations are "regular" */


LocalSymbols(cUnparserRegularOps) {
  cUnparserRegularOps := [ ["+"," + "], ["-"," - "], ["*"," * "],
                       ["/"," / "], [":="," = "], ["=="," == "],
                       ["=?"," == "], ["!=?"," != "], ["<=?"," <=? "],
                       [">=?"," >= "], ["<?"," < "], [">?"," > "],
                       ["And?"," && "], ["Or?"," || "], [">>", " >> "],
                       [ "<<", " << " ], [ "&", " & " ], [ "|", " | " ],
                       [ "%", " % " ], [ "^", " ^ " ],
                     ];

  UnparseCRegularOps() := cUnparserRegularOps;
}; // LocalSymbols(cUnparserRegularOps)

        /* This is the template for "regular" binary infix operators:
100 # UnparseC(_x + _y, _p) <-- UnparseCBracketIf(p<?PrecedenceGet("+"), ConcatStrings(UnparseC(x, LeftPrecedenceGet("+")), " + ", UnparseC(y, RightPrecedenceGet("+")) ) );
        */

        /* unary addition */
100 # UnparseC(+ _y, _p) <-- UnparseCBracketIf(p<?PrecedenceGet("+"), ConcatStrings(" + ", UnparseC(y, RightPrecedenceGet("+")) ) );

        /* unary subtraction */
100 # UnparseC(- _y, _p) <-- UnparseCBracketIf(p<?PrecedenceGet("-"), ConcatStrings(" - ", UnparseC(y, RightPrecedenceGet("-")) ) );

        /* power's argument is never bracketed but it must be put in braces. */
100 # UnparseC(_x ^ _y, _p) <-- UnparseCBracketIf(p<=?PrecedenceGet("^"), ConcatStrings("pow(", UnparseC(x, UnparseCMaxPrec()), ", ", UnparseC(y, UnparseCMaxPrec()), ")" ) );

100 # UnparseC(If(_pred)_body, _p) <-- "If("~UnparseC(pred,60000)~") "~UnparseC(body);
100 # UnparseC(_left Else _right, _p) <-- UnparseC(left)~" Else "~UnparseC(right);


LocalSymbols(cUnparserMathFunctions) {
  cUnparserMathFunctions :=
    [
      ["Sqrt","sqrt"],
      ["Cos","cos"],
      ["Sin","sin"],
      ["Tan","tan"],
      ["Cosh","cosh"],
      ["Sinh","sinh"],
      ["Tanh","tanh"],
      ["Exp","exp"],
      ["Ln","log"],
      ["ArcCos","acos"],
      ["ArcSin","asin"],
      ["ArcTan","atan"],
      ["ArcCosh","acosh"],
      ["ArcSinh","asinh"],
      ["ArcTanh","atanh"],
      ["Maximum","max"],
      ["Minimum","min"],
      ["Abs","fabs"],
      ["Floor","floor"],
      ["Ceil","ceil"],
    ["!","factorial"]
    ];

  UnparseCMathFunctions() := cUnparserMathFunctions;

}; // LocalSymbols(cUnparserMathFunctions)

/* Precedence of 120 because we'd like to process some special functions like pow() first */


120 # UnparseC(expr_Function?, _p)_(ArgumentsCount(expr)=?2 And? Contains?(AssocIndices(UnparseCRegularOps()), Type(expr)) ) <--
      UnparseCBracketIf(p<?PrecedenceGet(Type(expr)), ConcatStrings(UnparseC(FunctionToList(expr)[2], LeftPrecedenceGet(Type(expr))), UnparseCRegularOps()[Type(expr)], UnparseC(FunctionToList(expr)[3], RightPrecedenceGet(Type(expr))) ) );


/* Sin, Cos, etc. and their argument is always bracketed */

120 # UnparseC(expr_Function?, _p) _
      (ArgumentsCount(expr)=?1 And? Contains?(AssocIndices(UnparseCMathFunctions()), Type(expr)) ) <--
      ConcatStrings(UnparseCMathFunctions()[Type(expr)], "(", UnparseC( FunctionToList(expr)[2], UnparseCMaxPrec()),")" );

/* functions */

/* Unknown function, precedence 200. Leave as is, never bracket the function itself and bracket the argumentPointer(s) automatically since it's a list. Other functions are precedence 100 */

UnparseCArgs(list_List?) <--
{
  Local(i,nr,result);
  result:="";
  nr:=Length(list);
  For (i:=1,i<=?nr,i++)
  {
    result:=result~UnparseC(list[i]);
    Decide(i<?nr, result:=result~", ");
  };
  result;
};


200 # UnparseC(_x, _p)_(Function?(x)) <--
{
  ConcatStrings(Type(x), "(", UnparseCArgs(Rest(FunctionToList(x))),")" );
};

/* Complex numbers */
100 # UnparseC(Complex(0, 1), _p) <-- "I";
100 # UnparseC(Complex(_x, 0), _p) <-- UnparseC(x, p);
110 # UnparseC(Complex(_x, 1), _p) <-- UnparseC(x+Hold(I), p);
110 # UnparseC(Complex(0, _y), _p) <-- UnparseC(Hold(I)*y, p);
120 # UnparseC(Complex(_x, _y), _p) <-- UnparseC(x+Hold(I)*y, p);

/* Some special functions: Mod */

100 # UnparseC(Modulo(_x, _y), _p) <-- UnparseCBracketIf(p<?PrecedenceGet("/"), ConcatStrings(UnparseC(x, PrecedenceGet("/")), " % ", UnparseC(y, PrecedenceGet("/")) ) )
;

/* Indexed expressions are never bracketed */
// the rule with [ ] seems to have no effect?
//100 # UnparseC(_x [ _i ], _p) <-- ConcatStrings(UnparseC(x, UnparseCMaxPrec()), "[", UnparseC(i, UnparseCMaxPrec()), "]");
100 # UnparseC(Nth(_x, _i), _p) <-- ConcatStrings(UnparseC(x, UnparseCMaxPrec()), "[", UnparseC(i, UnparseCMaxPrec()), "]");

LocalSymbols(cindent) {
  cindent:=1;

  NlIndented():=
  {
    Local(result);
// carriage return, so needs to start at the beginning of the line
    result:=
"
";
    Local(i);
    For(i:=1,i<?cindent,i++)
    {
      result:=result~"  ";
    };
    result;
  };
  CIndent() :=
  {
  (cindent++);
  "";
  };
  CUndent() :=
  {
  (cindent--);
  "";
  };
}; // LocalSymbols(cindent)

UnparseCStatement(_x) <-- UnparseC(x) ~ ";" ~ NlIndented();

120 # UnparseC(_x,_p)_(Type(x) =? "Block") <--
{
  Local(result);
  result:=CIndent()~"{"~NlIndented();
  ForEach(item,Rest(FunctionToList(x)))
  {
    result:=result~UnparseCStatement(item);
  };
  result:=result~"}"~CUndent()~NlIndented();
  result;
};

120 # UnparseC(For(_from,_to,_step)_body,_p) <--
  "for(" ~ UnparseC(from,UnparseCMaxPrec()) ~ ";"
        ~ UnparseC(to,UnparseCMaxPrec()) ~ ";"
        ~ UnparseC(step,UnparseCMaxPrec()) ~ ")"
        ~ CIndent() ~ NlIndented()
        ~ UnparseCStatement(body) ~ CUndent();

120 # UnparseC(While(_pred)_body, _p) <--
        "while(" ~ UnparseC(pred,UnparseCMaxPrec()) ~ ")"
        ~ CIndent() ~ NlIndented()
        ~ UnparseCStatement(body) ~ CUndent();

%/mathpiper



%mathpiper_docs,name="UnparseC",categories="Programming Functions;Input/Output"
*CMD UnparseC --- export expression to C++ code
*STD
*CALL
        UnparseC(expr)
        
*PARMS

{expr} -- expression to be exported

*DESC

{UnparseC} returns a string containing C++ code that attempts to implement the MathPiper expression {expr}. Currently the exporter handles most expression types but not all.

*E.G.

In> UnparseC(Sin(a1)+2*Cos(b1));
Result: "sin(a1) + 2 * cos(b1)";

*SEE UnparseMath2D, UnparseLatex, CUnparsable?
%/mathpiper_docs






%mathpiper,name="UnparseC",subtype="automatic_test"

Verify(
UnparseC(Hold(Cos(A-B)*Sin(a)*func(b,c,d*(e+Pi))*Sqrt(Abs(C)+D)-(g(a+b)^(c+d))^(c+d)))
,"cos(A - B) * sin(a) * func(b, c, d * ( e + Pi) ) * sqrt(fabs(C) + D) - pow(pow(g(a + b), c + d), c + d)"
);

Verify(
UnparseC(Hold({i:=0;While(i<?10){i++; a:=a+Floor(i);};}))
, "{
  i = 0;
  while(i <? 10)
    {
      ++(i);
      a = a + floor(i);
      }
    ;
    ;
  }
"
);

/* Check that we can still force numbers to be floats in stead of integers if we want to */
Verify(
UnparseC(Hold({i:=0.;While(i<?10.){i++; a:=a+Floor(i);};}))
, "{
  i = 0.;
  while(i <? 10.)
    {
      ++(i);
      a = a + floor(i);
      }
    ;
    ;
  }
"
);

%/mathpiper





%mathpiper,name="UnparseC",subtype="automatic_test"

/* Jitse's bug report, extended with the changes that do not coerce integers to floats automatically
   any more (just enter a dot and the number becomes float if that is what is intended).
 */
Verify(UnparseC(4), "4");
Verify(UnparseC(4.), "4.");
Verify(UnparseC(0), "0");
Verify(UnparseC(0.), "0.");

%/mathpiper